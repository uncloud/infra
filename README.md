# Infra ❤️

## Repository Structure
- [ansible](ansible) – contains all Ansible code used to automate installation and configuration of all components running on the instance
- [scripts](scripts) - contains scripts to render the ansible inventory and to run ansible for the environment
- [tmp](tmp) - contains example env file to make it easy to run ansible for an environemtn, from a specific Task or specific Role

## Before running Ansible
Set up a new user to be used by Ansible with the following commands:

```bash
# add a new user to the system and add it to the sudo group
$ adduser --disabled-password --gecos "" uncloud
$ adduser uncloud sudo

# enable user to have passwordless sudo
$ echo 'uncloud ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/uncloud

# add SSH keys to the user
$ mkdir /home/uncloud/.ssh
$ touch /home/uncloud/.ssh/authorized_keys

# add your public keys to /home/uncloud/.ssh/authorized_keys

# fix file permissions
$ chown -R uncloud:uncloud /home/uncloud/.ssh
```

## Secrets Management
We are using [ansible-vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) to manage our secret data, with one master key.

The secret files are usually under `<role-name>/defaults/main/secrets.yaml`. This configuration is 

The file containing the secret is specified in the `ansible.cfg`.

If you need to create a new secret file, do:
```
$ ansible-vault create --vault-password-file <password file> foo.yml
```

If you need to edit a secret file, do:
```
$ ansible-vault edit --vault-password-file <password file> foo.yml
```

## Example usage
Copy the example env file inside its directory:

```
cp tmp/run_ansible.env.example tmp/run_ansible.env
```

Edit the tmp/run_ansible.env file and add the values that will make you next ansible run useful:

```
export ENVIRONMENT="uncloud1"
export TAGS="all"
#export TASK=""
export server_fqdn="REMOTE_IP_OR_FQDN"
export domain="example.com"
```

Run ansible with:

```
./scripts/run_ansible.sh
```

If you want to run a specific role, you can do that by setting the TAG env var, so instead of all, choose the role tag (or a comma separated list).

You can also run the playbook starting at a specific Task, setting the TASK env var to the Task name. Just edit the tmp/run_ansible.env file to set those values for the scripts.

```
export TAGS="common,nextcloud"
export TASK="Install Docker"
```

## Backup
Backup from services (data and database) are made every night to [Premiumize](https://www.premiumize.me/) and [Hetzner StorageBox](https://docs.hetzner.com/robot/storage-box).

The backups are encrypted with [age](https://github.com/FiloSottile/age) and stored for 7 days.

## Removing a service

### Docker Compose
Removing a Docker Compose service is usually done by following the steps below:
- Configure the `community.docker.docker_compose` task with `state: absent`, `remove_images: all` and `remove_volumes: yes`
- Run Ansible
- Configure the other tasks to clean up files, directories etc, generally using `state: absent`
- Run Ansible
- Delete the role code and commit

## Running locally
You can use the provided [Vagrant configuration](Vagrantfile) to run the infrastructure locally.

Install [Vagrant](https://www.vagrantup.com/docs/installation) and a virtual machine provider, like VirtualBox.

Start the virtual machine:
```
$ vagrant up
```

Edit the `server_fqdn` and `domain` in your `run_ansible.env`. They should be:
```
...
export server_fqdn="192.168.56.10"
export domain="local"
```

Run Ansible with Vagrant's "insecure key":
```
$ ./scripts/run_ansible.sh --key-file "$PWD/.vagrant/machines/default/virtualbox/private_key"
```

To access HTTPS services, add a line on your `/etc/hosts` file with the `192.168.56.10` IP and any services using the `.local` domain, like the following:
```
$ cat /etc/hosts

192.168.56.10 ots.local
192.168.56.10 nextcloud.local
192.168.56.10 libretranslate.local
```

A self-signed certificate error will appear in your browser, since the reverse-proxy will generate a local certificate. Just ignore it and move on!