# Nextcloud

## Backup
A backup of the Nextcloud data is done every day at 22h CET. The backups are encrypted and stored locally and in a remote storage. The backups are kept for 7 days.

You can fine more about the backup on the `tasks/main.yml` and `templates/backup.sh.j2`.

To restore the backup, follow the [Nextcloud documentation](https://docs.nextcloud.com/server/21/admin_manual/maintenance/restore.html).

## Restoring the backup
Setup a fresh Nextcloud instance.

Get the `data` and `db` backup files somewhere in the file system.

Create a `age.key` file with the secret `age` key and decrypt the files with:
```
$ age -d -i age.key -o backup-data.tar.gz backup-data.tar.gz.age
$ age -d -i age.key -o db.pgdump db.pgdump.age
```

Remove the `age.key` and encrypted backup files:
```
$ rm age.key backup-data.tar.gz.age db.pgdump.age
```

Enable Nextcloud in maintenance mode:
```
$ docker exec --user www-data app php occ maintenance:mode --on
```

Copy the backup data file:
```
$ docker cp backup-data.tar.gz nextcloud_app_1:/
```

Extract the backup file:
```
$ docker exec --workdir / nextcloud_app_1 tar xzf /backup-data.tar.gz
```

Delete the backup file:
```
$ docker exec --workdir / nextcloud_app_1 rm backup-data.tar.gz
```

Copy the backup db file:
```
$ docker cp db.pgdump nextcloud_db_1:/
```

Delete the `nextcloud` database:
```
$ docker exec nextcloud_db_1 /bin/bash -c 'PGPASSWORD=$POSTGRES_PASSWORD psql -h localhost -U $POSTGRES_USER -d template1 -c "DROP DATABASE \"nextcloud\";"'
```

Create the `nextcloud` database:
```
$ docker exec nextcloud_db_1 /bin/bash -c 'PGPASSWORD=$POSTGRES_PASSWORD psql -h localhost -U $POSTGRES_USER -d template1 -c "CREATE DATABASE \"nextcloud\";"'
```

Import the database:
```
$ docker exec nextcloud_db_1 /bin/bash -c 'PGPASSWORD=$POSTGRES_PASSWORD psql -h localhost -U $POSTGRES_USER -d nextcloud -f /db.pgdump'
```

Delete the database file:
```
$ docker exec nextcloud_db_1 rm /db.pgdump
```

Disable Nextcloud in maintenance mode:
```
$ docker exec --user www-data app php occ maintenance:mode --off
```

Remove the backup files locally:
```
$ rm backup-data.tar.gz db.pgdump
```

## Two-Factor authentication
Two-Factor authentication is enabled by default and enforced to all users. Please enable and make a copy of your security backup codes [here](https://nextcloud.uncloud.do/settings/user/security).

## Security
All the files are currently stored with the Nextcloud server itself.

We don't enable [Nextcloud encryption](https://docs.nextcloud.com/server/21/user_manual/en/files/encrypting_files.html?highlight=encryption). This feature only protects file access to external storage _if_ external storage is used, which is currently not.

We don't access your files, however we theoretically can because we are administrators with full access. Even with the [Nextcloud encryption](https://docs.nextcloud.com/server/21/user_manual/en/files/encrypting_files.html?highlight=encryption) enabled, administrators could retrieve the encryption keys and decrypt the files. You'll need to trust us one this one.

If _really_ want to protect your files (even from us!), you need to enable end-to-end encryption. This can be achieved with tools like [Cryptomator](https://cryptomator.org).

We currently don't support [Nextcloud End-to-End Encryption App](https://apps.nextcloud.com/apps/end_to_end_encryption), it presented some inconsistencies in our tests.

## Upgrade
Follow the instructions [here](https://github.com/nextcloud/docker#update-to-a-newer-version).