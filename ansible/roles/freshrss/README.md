# FreshRSS

## Restoring the backup
Setup a fresh FreshRSS instance.

Get the backup file somewhere in the file system.

Create a `age.key` file with the secret `age` key and decrypt the file with:
```
$ age -d -i age.key -o backup.tar.gz backup.tar.gz.age
```

Remove the `age.key` and encrypted backup file:
```
$ rm age.key backup.tar.gz.age
```

Copy the decrypted backup file inside the Docker container:

```
$ docker cp backup.tar.gz freshrss_freshrss_1:/
```

Extract the backup file:
```
$ docker exec -ti --workdir / freshrss_freshrss_1 tar xzf /backup.tar.gz
```

Remove the backup file on the container:
```
$ docker exec freshrss_freshrss_1 rm /backup.tar.gz
```

Remove the backup file locally:
```
$ rm backup.tar.gz
```
